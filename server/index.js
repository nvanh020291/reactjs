const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");

const app = express();
const httpServer = createServer(app);
const socketServer = new Server(httpServer, {
    cors: {
      origin: "http://localhost:3001"
    }
});

  socketServer.on("connection", (socket) => {
    socket.on("clientSendMessage", (data) => {
        socketServer.emit("serverSendMessage", { data });
    });
    //socket.emit("server-hello", "world server");
    socket.on("disconnect", () => {
      console.log("Client disconnected");
    });
  });

httpServer.listen(3000 , () => {
    //console.log("Server listen at port 3000");
});