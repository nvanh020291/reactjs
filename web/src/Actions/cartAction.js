
import processApi from '../Api/processApi' ; 

export const getProducts = (page , limit) => async (dispatch) => {

    return  processApi('GET' , `product?page=${page}&limit=${limit}` , null).then(res => {

        dispatch({ type: 'GET_ALL_PRODUCT', payload: res.data })

    }); 
}

export const getProductDetail = (output , productId) => async () => {
    
    return processApi('GET' , `product/${productId}`).then(res => {

        output(res.data)

    }); 
}

export const addCart = (payload) => {
    
    localStorage.setItem('carts', JSON.stringify(payload));

    return {
        type : 'ADD_CART' , 
        payload : payload
    }
}

export const deleteItem =  (payload) => {
    return {
        type : 'DELETE_ITEM' , 
        payload : payload,
    }
}
 
