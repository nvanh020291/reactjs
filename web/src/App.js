import React from 'react';
import './App.css';
import Products from './Components/Products';
import Cart from './Components/Cart';
import Chat from './Components/Chat';
import ProductDetail from './Components/ProductDetail';
import Navbar from './Components/Navbar';
import { Routes, Route, BrowserRouter } from "react-router-dom";
class App extends React.Component {
  render()  {
    return (
      <BrowserRouter>
            <div className="App">
             <Navbar/>
               <Routes>
                   <Route exact path="/" element={<Products />}/>
                   <Route path="/cart" element={<Cart />}/>
                   <Route path="/chat" element={<Chat />}/>
                   <Route path="product/:productId" element={<ProductDetail />} />
                 </Routes>
            </div>
      </BrowserRouter> 
   );
  }
}
export default App;
