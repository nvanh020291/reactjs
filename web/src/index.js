import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { Provider } from 'react-redux';
import cartReducer from './Reducers/cartReducer';
import { configureStore } from '@reduxjs/toolkit'
import thunkMiddleware from 'redux-thunk'
import {applyMiddleware} from 'redux'

const composedEnhancer = applyMiddleware(thunkMiddleware);

const store = configureStore({ reducer: cartReducer ,  composedEnhancer})

ReactDOM.render(
	<Provider store={store}>
    <App />
  </Provider>,
	document.getElementById('root'));