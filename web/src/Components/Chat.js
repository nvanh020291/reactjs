import '../chat.css';
import React, { useState, useEffect, useRef } from "react";
import { io } from "socket.io-client";

function Chat(){

  const [message , setMessage]  =  useState('') ;

  const [mess, setMess]  =  useState([]) ;

  const socketRef =   useRef();

  const messagesEnd = useRef();
  
  useEffect(() => {

    socketRef.current =  io("http://localhost:3000/");

    socketRef.current.on('serverSendMessage', dataGot => {
      setMess(oldMsgs => [...oldMsgs, dataGot.data]);
      scrollToBottom(); 
      return () => {
        socketRef.current.disconnect();
      };
  
    })
    
  },[]);

  const sendMessage = () => {
    const data = {
      message : message,
      sockerId : socketRef.current.id
    }

    if(data.message.trim().length  > 0){
       socketRef.current.emit("clientSendMessage", data);
       setMessage('');
    }
  
  }
  const onEnterPress = (e) => {
    if(e.keyCode === 13 && e.shiftKey === false) {
      sendMessage();
    }
  }
  
  const handleChange = (e) => {
    setMessage(e.target.value)
  }

  const scrollToBottom = () => {
    messagesEnd.current.scrollIntoView({ behavior: "smooth" });
  }

  const listMess = mess.map((m , index) =>
    <div key={index} className={`chat-item ${m.sockerId === socketRef.current.id ?   'your-message' : 'other-people'}`}>
    {m.message}
    </div>
  );

  return (
        <div>
          <div className="form-group">
          <div className="box-chat">
              <div className="box-chat_message">{listMess}
              <div style={{ float:"left", clear: "both" }}
                ref={messagesEnd}>
            </div>
              </div>
              <div className="send-box">
                  <textarea  onKeyDown={onEnterPress} onChange={handleChange}  value={message} className="form-control" placeholder="Enter ...">dlkasdj</textarea>
              </div>
          </div>
          </div>
          <div className="form-group">
          <button className="form-control btn-primary" onClick={() => sendMessage()}>Send</button>
          </div>
        </div>
  );
}

export default Chat
 