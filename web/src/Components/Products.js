import React from 'react';
import Pagination from 'react-bootstrap/Pagination';
import { connect } from 'react-redux';
import {getProducts, addCart} from '../Actions/cartAction';
import { Link } from 'react-router-dom';


class Products extends React.Component {
  state = {
    page : 1,
    limit: 3,
  }

  componentDidMount() {

    this.props.getProducts(this.state.page  , this.state.limit)  ;

  }

  getListProduct(page , limit) {

    this.props.getProducts(page,limit) ;

    this.setState({
      page :  page
    })

  }

  render()  {

    let listPage = [];
  
    let totalPage  =  Math.round(6/this.state.limit) ;

    for (let i = 1; i <= totalPage ; i++) {
        if(i === this.state.page){
          listPage.push(<Pagination.Item  key={i}  active onClick={() => this.getListProduct(i ,this.state.limit)}>{i}</Pagination.Item>); 
        }else{
          listPage.push(<Pagination.Item  key={i} onClick={() => this.getListProduct(i ,this.state.limit)}>{i}</Pagination.Item>); 
        }
    }
    return (
      <div className="container py-5">
        <div className="row text-center text-white mb-5">
          <div className="col-lg-7 mx-auto">
           
            <h1 className="display-4">Product List</h1>
          </div>
        </div>
        <div className="row">
        <div>
        <Pagination>
          <Pagination.First />
          <Pagination.Prev />
             {listPage}
          <Pagination.Next />
          <Pagination.Last />
        </Pagination>
        </div>
          <div className="col-lg-8 mx-auto">            
            {
              this.props.products.map((product, index) =>
              // Only do this if items have no stable IDs
                <ul className="list-group shadow" key={product.id}>
                      <li className="list-group-item" >
                          <div className="media align-items-lg-center flex-column flex-lg-row p-3">
                            <div className="media-body order-2 order-lg-1">
                              <h5 className="mt-0 font-weight-bold mb-2"><Link to={`product/${product.id}`}>{product.name}</Link></h5>
                              <p className="font-italic text-muted mb-0 small">{product.des}</p>
                              <div className="d-flex align-items-center justify-content-between mt-1">
                                <h6 className="font-weight-bold my-2">{product.price}$</h6>
                              </div>
                              <button type="button" className="btn btn-success" onClick={() => this.props.addCart(product , 1)}>add</button>
                            </div>
                          </div> 
                      </li>
                </ul>
              )}        
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  //localStorage.setItem('carts', JSON.stringify(state.cartDetail));

  return { 
    products: state.products,
    cartDetail: state.cartDetail,
    totalPrice: state.totalPrice,
    numberCart: state.numberCart
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    getProducts: (page,limit) => dispatch(getProducts(page,limit)),
    addCart : (product) => dispatch(addCart(product))
  }
}

export default  connect(mapStateToProps , mapDispatchToProps)(Products)
