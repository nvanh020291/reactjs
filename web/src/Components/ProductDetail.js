import React  , { useState , useEffect } from 'react';
import {
  useParams,
} from "react-router-dom";

import  { getProductDetail } from '../Actions/cartAction';

export default function ProductDetail(){

  let params = useParams();

  const [productDetail,setProductDetail] = useState([]) ;

  const fetchProducts  =  getProductDetail(setProductDetail , params.productId) ; 

  useEffect(() => {
    fetchProducts();
  },[fetchProducts])

  return (
    <div>    
          <div className="details col-md-6">
          <h3 className="product-title">{productDetail.name}</h3>
          <p className="product-description">{productDetail.des}.</p>
          <h4 className="price">current price: <span>${productDetail.price}</span></h4>
          <div className="action">
            <button className="add-to-cart btn btn-default" type="button">add to cart</button>
          </div>
        </div>
    </div>
  );
}
