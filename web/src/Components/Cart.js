import React  from 'react';
import '../cart.css';
import { connect } from 'react-redux';
import { addCart , deleteItem} from '../Actions/cartAction';
import { Link } from 'react-router-dom';

class Cart extends React.Component {

  /* componentDidMount() {
    console.log('componentDidMount') ;
  }

  componentWillUnmount() {
    console.log('componentWillUnmount') ;
  }

  shouldComponentUpdate(){
    return true  ; 
    console.log('shouldComponentUpdate') ;
  }
  */
  componentDidUpdate(){
   // console.log('componentDidUpdate') ;
  }

  render()  {

    let countCartDetail =  this.props.cartDetail.length;  
    
    return (
      <div className="card">
        <div className="row">
          <div className="col-md-8 cart">
            <div className="title">
              <div className="row">
                <div className="col"><h4><b>Shopping Cart</b></h4></div>
                <div className="col align-self-center text-right text-muted">Total product {this.props.numberCart}</div>
              </div>
            </div>   
            { 
              countCartDetail > 0 ? 
                this.props.cartDetail.map((product, index) => 
                //console.log(product) ;
                  <div className="row border-top border-bottom" key={index}>
                    <div className="row main align-items-center">
                      <div className="col-2"><img alt="adaslkj" className="img-fluid" src="https://i.imgur.com/pHQ3xT3.jpg" /></div>
                      <div className="col">
                        <div className="row text-muted">{product.name}</div>
                      </div>
                      <div className="col">
                           {product.quantity}
                        </div>
                      <div className="col">€ {product.price} <span onClick={() => this.props.deleteItem(product)} className="close">✕</span></div>
                    </div>
                  </div>
              ) : 'Cart empty' 
             }
            
            <div className="back-to-shop"><Link to="/">Back to shop</Link></div>
          </div>
          <div className="col-md-4 summary">
            <div><h5><b>Summary</b></h5></div>
            <hr />
            <div className="row" style={{borderTop: '1px solid rgba(0,0,0,.1)', padding: '2vh 0'}}>
              <div className="col">TOTAL PRICE</div>
              <div className="col text-right">€ {this.props.totalPrice}</div>
            </div>
            <button className="btn">CHECKOUT</button>
          </div>
        </div>
      </div>
    );
    
  }
}

const mapStateToProps = (state) => {

  //localStorage.setItem('carts', JSON.stringify(state.cartDetail));

  //state.cartDetail = JSON.parse(localStorage.getItem('carts'));

  return { 
    cartDetail: state.cartDetail,
    totalPrice: state.totalPrice,
    numberCart: state.numberCart
  }
}

const mapDispatchToProps = (dispatch) => {

  return {
    addCart : (product,quantity) => dispatch(addCart(product , quantity)),
    deleteItem : (productId) => dispatch(deleteItem(productId))
  }
}

export default  connect(mapStateToProps , mapDispatchToProps)(Cart)
 