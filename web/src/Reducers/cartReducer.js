import produce from 'immer';
const initialState = {
    numberCart : 0,
    totalPrice : 0 ,
    products : [],
    cartDetail : []
  }

export  default function cartReducer(state  =  initialState, action){

    switch (action.type) {
        case 'GET_ALL_PRODUCT' :{
            return {
                ...state,
                products : action.payload
            }
        }
        case 'DELETE_ITEM' : {
            const cartDetail = state.cartDetail.filter((product) => {
                return product.id !== action.payload.id
              })
            return{
                ...state,
                cartDetail : cartDetail , 
                numberCart : state.numberCart - action.payload.quantity ,
                totalPrice : state.totalPrice - action.payload.price * action.payload.quantity
    
            }
        }
        case 'ADD_CART' : {
            // check if exist product in cart or not
           // console.log(state.cartDetail) ;
            if(state.numberCart > 0){ 
                const index = state.cartDetail.findIndex(todo => todo.id ===  action.payload.id);  
                if(index !== -1){
                    
                    return produce(state, draft => {
                         draft.cartDetail[index].quantity  += 1 ;
                         draft.totalPrice =  state.totalPrice + action.payload.price ;  
                         draft.numberCart =  state.numberCart + 1   
                     });

                }else{
                    let cart = {
                        id:action.payload.id,
                        quantity:1,
                        name:action.payload.name,
                        price:action.payload.price
                    }
                    return{
                        ...state,
                        cartDetail : [...state.cartDetail , cart],
                        numberCart : state.numberCart + 1 ,
                        totalPrice : state.totalPrice + action.payload.price
                    }
                }    
            }else{
                let cart = {
                    id:action.payload.id,
                    quantity:1,
                    name:action.payload.name,
                    price:action.payload.price
                }
                return{
                    ...state,
                    cartDetail : [...state.cartDetail , cart],
                    numberCart : 1 ,
                    totalPrice : state.totalPrice + action.payload.price
        
                }
            }
        }
        default : 
          return state   
      }
      
}